#!/usr/bin/env python
#coding=utf-8

import logging
import time
import traceback
import xlrd
import csv
import sys
import re

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

def xls2csv(xls, target):
    try:
        logging.info("Start converting: From '" + xls + "' to '" + target + "'. ")
        start_time = time.time()
        sh = xlrd.open_workbook(xls).sheet_by_index(0) #只转换第一个sheet，需要转换更多sheet请参考xlrd的api

        csvFile = open(target, 'wb')
        wr = csv.writer(csvFile, quoting=csv.QUOTE_MINIMAL) #field中包含需要转移的字符时，field才会被 "" 包起来

        for row in xrange(sh.nrows):
            newValues = []
            for s in sh.row_values(row):
                if isinstance(s, unicode): #如果fields是unicode则转化为utf8，否则转换为str
                    strValue = (str(s.encode("utf-8")))
                else:
                    strValue = (str(s))

                if bool(re.match("^-?([0-9]+)\.0$", strValue)): #int
                    strValue = int(float(strValue))
                elif bool(re.match("^-?([0-9]+)\.([0-9]+)$", strValue)): #float
                    strValue = float(strValue)
                elif bool(re.match("^-?([0-9]+)\.([0-9]+)e\+([0-9]+)$", strValue)): #long，excel有时会自动转换超过11位的数为科学计数法
                    strValue = int(float(strValue))
                newValues.append(strValue)
            wr.writerow(newValues)

        csvFile.close()
        logging.info("Finished in %s seconds", time.time() - start_time)
    except Exception as e:
        print (str(e) + " " +  traceback.format_exc())

xls2csv(sys.argv[1], sys.argv[2])
